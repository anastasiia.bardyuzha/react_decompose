## Task
Split the `App.js` into smaller components basing on CSS blocks.
- Create a sub folder in `components` per component and put `js` and `css` files there.
- There should be 3 - 5 new components.
